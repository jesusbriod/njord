import express from 'express';
import * as dao from '../company/company.dao';
import * as competitor from '../certificate/competitor';
let Web3 = require('web3');
//let crypto = require('crypto');
//let Tx = require ('ethereumjs-tx');

const router = express.Router();

export default function(){
  router.use((req, res, next) => {
    res.append('Last-Modified', Date.now());
    next();
  })
  .get('/competitor', (req, res, next) => {
    try{
      console.log(req.param("name"));
      let name = String(req.param("name"));
      dao.findByName(name)
        .then((rst) => {
          let certificateCompetitor = competitor.getCertificate(rst.competitorTx);
          console.log(rst.name);
          res.status(200).json(certificateCompetitor);
        })
        .catch(next);

    } catch(err) {
      next(err);
    }
  })
  .get('/winner', (req, res, next) => {
    try{
      console.log(req.param("name"));
      let name = String(req.param("name"));
      dao.findByName(name)
        .then((rst) => {
          let certificateCompetitor = competitor.getCertificate(rst.winnerTx);
          console.log(rst.name);
          res.status(200).json(certificateCompetitor);
        })
        .catch(next);

    } catch(err) {
      next(err);
    }
  })
  .get('/competitorStatus', (req, res, next) => {
    try{
      console.log(req.param("name"));
      //let name = String(req.param("name"));

      //const web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/Txi0ymKioUjkZplfGOyb"));
      const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/Txi0ymKioUjkZplfGOyb"));

      if(!web3.isConnected()) {
        // show some dialog to ask the user to start a node
        console.log('false');
      } else {
        console.log('connected');
      }

      dao.findAllOneColumn()
        .then((cmp) => {
          console.log("CMP: ",cmp[0].name);
          console.log("cmp le:", cmp.length);

          for (var i = 0; i < cmp.length; i++) {
            console.log("cmp: ", cmp[i].name);

            dao.findByName(cmp[i].name)
              .then((rst) => {
                try {
                  console.log("competitor tx: ",rst.competitorTx);
                  var receipt = web3.eth.getTransaction(rst.competitorTx);
                  console.log("receipt: ", receipt);
                  if(receipt.blockNumber!=null){
                    dao.update(cmp[i].name, {"competitorStatus": "Complete"})
                      .then((cmp) => {
                        console.log("CMP: ",cmp);
                        //let msg = { 'message': `Company updated: ${cmp.name}` };

                      } )
                      .catch((err) => {
                        if(err){
                          if(err.code === 11000){
                            //let msg = `Company name is duplicated: ${company.name}`;
                            console.log("err: ",err.code);


                          }
                          next(err);
                        }
                      });
                  } else {
                    dao.update(cmp[i].name, {"competitorStatus": "Pending"})
                      .then((cmp) => {
                        console.log("CMP: ",cmp);
                        //let msg = { 'message': `Company updated: ${cmp.name}` };

                      } )
                      .catch((err) => {
                        if(err){
                          if(err.code === 11000){
                            //let msg = `Company name is duplicated: ${company.name}`;
                            console.log("err: ",err.code);


                          }
                          next(err);
                        }
                      });

                  }

                } catch (e) {
                  dao.update(cmp[i].name, {"competitorStatus": "Error"})
                    .then((cmp) => {
                      console.log("CMP: ",cmp);
                      //let msg = { 'message': `Company updated: ${cmp.name}` };

                    } )
                    .catch((err) => {
                      if(err){
                        if(err.code === 11000){
                          //let msg = `Company name is duplicated: ${company.name}`;
                          console.log("err: ",err.code);


                        }
                        next(err);
                      }
                    });




          }
        } )
        .catch((err) => {
          if(err){
            if(err.code === 11000){
              console.log("err: ",err.code);
            }
            next(err);
          }
        });





          }

          res.status(200).json("ok");
        })
        .catch(next);

    } catch(err) {
      next(err);
    }
  });

  return router;
}

/*
.get('/:id?', (req, res, next) => {
  try{
    dao.findCompany(req.params.id)
      .then((rst) => {
        res.status(200).json(rst);
      })
      .catch(next);
  } catch(err) {
    next(err);
  }
});

*/
