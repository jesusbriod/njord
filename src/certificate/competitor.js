let Web3 = require('web3');
let crypto = require('crypto');
let Tx = require ('ethereumjs-tx');

import * as dao from '../company/company.dao';




export function setCompetitorCertificate(name, input){
  let dataHash;
  let _input = input;

  console.log("input: ", input);
  // Connect to public Ethereum node

  // const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/Txi0ymKioUjkZplfGOyb"));
  const web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/Txi0ymKioUjkZplfGOyb"));


  if(!web3.isConnected()) {
    // show some dialog to ask the user to start a node
    console.log('false');
  } else {
    console.log('connected');
  }
  let dataCertificate = String(_input);
  let cipher = crypto.createCipher('aes-128-cbc', '@Axtel#2017$2017.');
  let crypted = cipher.update(dataCertificate, 'utf8', 'hex');
  crypted += cipher.final('hex');
  console.log("dataCertificate enviados en la transaccion: ", dataCertificate);
  console.log("Criptada: ",crypted); //34feb914c099df25794bf9ccb85bea72

  let gasPrice = web3.eth.gasPrice.toNumber()*2;
  let gasPriceHex = web3.toHex(gasPrice);
  let gasLimitHex = web3.toHex(80000);
  let ethCoinbase='0x769C9B54741890A9F39E917B3c9418B275228F65';
  let nonce = web3.eth.getTransactionCount(ethCoinbase);
  let nonceHex = web3.toHex(nonce);
  let rawTx = {
    nonce: nonceHex,
    gasPrice: gasPriceHex,
    gasLimit: gasLimitHex,
    data: crypted,
    from: ethCoinbase,
    to: '0x444843e0239c0b0cc259f52a0e8c7afe55adf3e9'
  };

  let privateKey = new Buffer('4a0cd0314255aa0585b13c444c13181056c29412b294c9be768f01f655999031','hex');
  let tx = new Tx(rawTx);
  tx.sign(privateKey);
  let serializedTx = tx.serialize();
  //let dataHash;

  web3.eth.sendRawTransaction('0x'+ serializedTx.toString('hex'), (err, hash) => {

    if (err) { dataHash=null; console.log(err); return; }

    // Log the tx, you can explore status manually with eth.getTransaction()
    console.log('ID de la transacción en la blockchain: ' + hash);
    dataHash = String(hash);
    //var receipt = web3.eth.getTransactionReceipt(hash);
    //console.log("Receipt, ",receipt);
    // Wait for the transaction to be mined
    //waitForTransactionReceipt(hash);
    dao.update(name, {"competitorTx": dataHash})
    .then((cmp) => {
      console.log("CMP: ",cmp);
      //let msg = { 'message': `Company updated: ${cmp.name}` };

    } )
    .catch((err) => {
      if(err){
        if(err.code === 11000){
          //let msg = `Company name is duplicated: ${company.name}`;
          console.log("err: ",err.code);


        }
      }
    });

    setTimeout(() => {

      //var receipt = web3.eth.getTransaction(dataHash);
      //console.log("Input Data Transaction: ",web3.toAscii(receipt.input));



    }, 1000);

  });




  return dataHash;
}

export function setWinnerCertificate(name, input){
  // Connect to public Ethereum node
  let _input = input;
  //const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/Txi0ymKioUjkZplfGOyb"));
  const web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/Txi0ymKioUjkZplfGOyb"));
  if(!web3.isConnected()) {
    // show some dialog to ask the user to start a node
    console.log('false');
  } else {
    console.log('connected');
  }
  let dataCertificate = String(_input);
  let cipher = crypto.createCipher('aes-128-cbc', '@Axtel#2017$2017.');
  let crypted = cipher.update(dataCertificate, 'utf8', 'hex');
  crypted += cipher.final('hex');
  console.log("dataCertificate enviados en la transaccion: ", dataCertificate);
  console.log("Criptada: ",crypted); //34feb914c099df25794bf9ccb85bea72

  let gasPrice = web3.eth.gasPrice.toNumber()*2;
  let gasPriceHex = web3.toHex(gasPrice);
  let gasLimitHex = web3.toHex(80000);
  let ethCoinbase='0x769C9B54741890A9F39E917B3c9418B275228F65';
  let nonce = web3.eth.getTransactionCount(ethCoinbase);
  let nonceHex = web3.toHex(nonce);
  let rawTx = {
    nonce: nonceHex,
    gasPrice: gasPriceHex,
    gasLimit: gasLimitHex,
    data: crypted,
    from: ethCoinbase,
    to: '0x444843e0239c0b0cc259f52a0e8c7afe55adf3e9'
  };

  let privateKey = new Buffer('4a0cd0314255aa0585b13c444c13181056c29412b294c9be768f01f655999031','hex');
  let tx = new Tx(rawTx);
  tx.sign(privateKey);
  let serializedTx = tx.serialize();
  let dataHash;
  web3.eth.sendRawTransaction('0x'+ serializedTx.toString('hex'), (err, hash) => {
    if (err) { dataHash=null; console.log(err); return; }

    // Log the tx, you can explore status manually with eth.getTransaction()
    console.log('ID de la transacción en la blockchain: ' + hash);
    dataHash = String(hash);
    //var receipt = web3.eth.getTransactionReceipt(hash);
    //console.log("Receipt, ",receipt);
    // Wait for the transaction to be mined
    //waitForTransactionReceipt(hash);
    dao.update(name, {"winnerTx": dataHash})
      .then((cmp) => {
        console.log("CMP: ",cmp);
        //let msg = { 'message': `Company updated: ${cmp.name}` };

      } )
      .catch((err) => {
        if(err){
          if(err.code === 11000){
            //let msg = `Company name is duplicated: ${company.name}`;
            console.log("err: ",err.code);


          }
        }
      });
    console.log('Procesando información... este proceso suele tardar hasta 1 minuto...');
    setTimeout(() => {

      //var receipt = web3.eth.getTransaction(dataHash);
      //console.log("Input Data Transaction: ",web3.toAscii(receipt.input));



    }, 30000);

  });



  return dataHash;
}

export function getCertificate(tx){
  // Connect to public Ethereum node
  //const web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/Txi0ymKioUjkZplfGOyb"));
  const web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/Txi0ymKioUjkZplfGOyb"));

  let _tx = tx;

  if(!web3.isConnected()) {
    // show some dialog to ask the user to start a node
    console.log('false');
  } else {
    console.log('connected');
  }
  let receipt = web3.eth.getTransaction(_tx);
  let Certification = web3.toAscii(receipt.input);
  console.log("Input Data Transaction: ",Certification);

  let decipher = crypto.createDecipher('aes-128-cbc', '@Axtel#2017$2017.');
  let dec = decipher.update(web3.toAscii(receipt.input),'hex','utf8');
  dec += decipher.final('utf8');

  let certificationInfo = dec+","+Certification;
  let certificatedata = new Array();
  certificatedata=certificationInfo.split(",");
  console.log("certificatedata: ",certificatedata);

  let certificateJson = {
    "name": certificatedata[0],
    "attorney": certificatedata[1],
    "email": certificatedata[2],
    "nationality": certificatedata[3],
    "hyperlink": certificatedata[4],
    "fileHash": certificatedata[5],
    "certificate": certificatedata[6],
    "idTx": _tx
  };
  //console.log("certificateJson: ", certificateJson);



  //console.log("Información desencriptada: ",dec);
  return certificateJson;
}
/*
export function findByName(name){
  return (CompanyModel.findOne({"name":name}));
}

export function update(name, company){
  return CompanyModel.update({"name":name}, company);
}*/
