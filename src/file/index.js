import express from 'express';
import os from 'os';
import fs from 'fs';
import multer from 'multer';
//import async from 'async';
//import nj from 'numjs';
import path from 'path';
import crypto from 'crypto';
import * as dao from '../company/company.dao';
import * as competitor from '../certificate/competitor';



export default function(){
  let router = express.Router();
  fs.mkdtemp(path.join(os.tmpdir(), 'njord-files'), (err, tmpdir) => {

		if (err) throw err;
		console.log("Temp dir: %s", tmpdir);



		const storage = multer.diskStorage({
			destination:
				(req, file, cb) => cb(null, tmpdir),
			filename: function(req, file, cb) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        cb(null, file.fieldname + '-' + Date.now() + '.'+extension);},
		});
		const upload = multer({ storage: storage });

		router.get('/', (req, res) => {
			res.writeHead(200, {'Content-Type': 'text/plain'});
			res.end('File System\n');
		})
		.post('/competitor',upload.single('image'),	(req, res, next) => {
			try {
				//let imgMatrix;
        //console.log("app: ",router);
				console.log(JSON.stringify(req.headers));
        //console.log("filex: ",req);
				const f = req.file;
        console.log("F: ",f);
				if(!f){
					return res.json({ success: false, message: "File not received" });
				}
				//const filepath = f.destination + '/' + f.filename;
				//console.log("Temp file: %s %s", filepath, f.mimetype);
				/*if (router.images.mimetypes.indexOf(f.mimetype) < 0) {
					throw new Error( 'Invalid file - please upload an image (.jpg, .png, .gif).');
				}*/
        let path = f.destination+'/'+f.filename;
				console.log("Reading");
        let certificateStatus;
        let fd = fs.createReadStream(path);
        let hashFile = crypto.createHash('sha1');
        hashFile.setEncoding('hex');
        fd.on('end', function() {
          hashFile.end();

          let hashFileAt = hashFile.read();
          console.log("HASH FILE: ",hashFileAt);
          // the desired sha1sum
          let company = req.body;
          dao.update(company.name, {"competitorHash": hashFileAt})
            .then((cmp) => {
              console.log("CMP: ",cmp);
              //let msg = { 'message': `Company updated: ${cmp.name}` };

            } )
            .catch((err) => {
              if(err){
                if(err.code === 11000){
                  //let msg = `Company name is duplicated: ${company.name}`;
                  console.log("err: ",err.code);


                }
                next(err);
              }
            });

            dao.findByName(company.name)
              .then((cmp) => {
                console.log("CMP j: ",cmp);
                //let msg = { 'message': `Company updated: ${cmp.name}` };
                if(cmp.competitorTx==null){
                  let dataCertificate = cmp.name+','+cmp.attorney+','+cmp.email+','+cmp.nationality+','+cmp.hyperlink+','+cmp.competitorHash;
                  console.log("dataCertificate: ", dataCertificate);
                  competitor.setCompetitorCertificate(company.name, dataCertificate);
                  certificateStatus=true;
                }
                else {
                  certificateStatus=false;
                }


              } )
              .catch((err) => {
                if(err){
                  if(err.code === 11000){
                    //let msg = `Company name is duplicated: ${company.name}`;
                    console.log("err: ",err.code);


                  }
                  next(err);
                }
              });



        });
        // read all file and pipe it (write it) to the hash object
        fd.pipe(hashFile);






        return res.json({ success: true, filename: f.filename, certificate: certificateStatus});

			} catch (err) {
				console.error("Try-catch Error %s", err);
				console.trace(err);
				return res.json({ success: false,  message : err.message });
			}
		})
    .post('/winner1',upload.single('image'),	(req, res, next) => {
			try {
				//let imgMatrix;
        //console.log("app: ",router);
				console.log(JSON.stringify(req.headers));
        //console.log("filex: ",req);
				const f = req.file;
        console.log("F: ",f);
				if(!f){
					return res.json({ success: false, message: "File not received" });
				}
				//const filepath = f.destination + '/' + f.filename;
				//console.log("Temp file: %s %s", filepath, f.mimetype);
				/*if (router.images.mimetypes.indexOf(f.mimetype) < 0) {
					throw new Error( 'Invalid file - please upload an image (.jpg, .png, .gif).');
				}*/
        let path = f.destination+'/'+f.filename;
				console.log("Reading");
        //let certificateStatus;
        let fd = fs.createReadStream(path);
        let hashFile = crypto.createHash('sha1');
        hashFile.setEncoding('hex');
        fd.on('end', function() {
          hashFile.end();

          let hashFileAt = hashFile.read();
          console.log("HASH FILE: ",hashFileAt);
          // the desired sha1sum
          let company = req.body;


            dao.findByName(company.name)
              .then((cmp) => {
                console.log("CMP j: ",cmp);
                //let msg = { 'message': `Company updated: ${cmp.name}` };
                if(cmp.winnerTx==null&&cmp.competitorTx!=null){
                  dao.update(company.name, {"winnerHash1": hashFileAt})
                    .then((cmp) => {
                      console.log("CMP: ",cmp);
                      //let msg = { 'message': `Company updated: ${cmp.name}` };

                    } )
                    .catch((err) => {
                      if(err){
                        if(err.code === 11000){
                          //let msg = `Company name is duplicated: ${company.name}`;
                          console.log("err: ",err.code);


                        }
                        next(err);
                      }
                    });
                    if(cmp.winnerHash2!=null){
                      let dataCertificate = cmp.name+','+cmp.attorney+','+cmp.email+','+cmp.nationality+','+cmp.hyperlink+','+cmp.winnerHash1+cmp.winnerHash2+cmp.winnerHash3;

                      competitor.setWinnerCertificate(company.name, dataCertificate);

                      


                    }
                }



              } )
              .catch((err) => {
                if(err){
                  if(err.code === 11000){
                    //let msg = `Company name is duplicated: ${company.name}`;
                    console.log("err: ",err.code);


                  }
                  next(err);
                }
              });
        });
        // read all file and pipe it (write it) to the hash object
        fd.pipe(hashFile);




        return res.json({ success: true, filename: f.filename/*, filepath: path*/});

			} catch (err) {
				console.error("Try-catch Error %s", err);
				console.trace(err);
				return res.json({ success: false,  message : err.message });
			}
		})
    .post('/winner2',upload.single('image'),	(req, res, next) => {
			try {
				//let imgMatrix;
        //console.log("app: ",router);
				console.log(JSON.stringify(req.headers));
        //console.log("filex: ",req);
				const f = req.file;
        console.log("F: ",f);
				if(!f){
					return res.json({ success: false, message: "File not received" });
				}
				//const filepath = f.destination + '/' + f.filename;
				//console.log("Temp file: %s %s", filepath, f.mimetype);
				/*if (router.images.mimetypes.indexOf(f.mimetype) < 0) {
					throw new Error( 'Invalid file - please upload an image (.jpg, .png, .gif).');
				}*/
        let path = f.destination+'/'+f.filename;
				console.log("Reading");
        //let certificateStatus;
        let fd = fs.createReadStream(path);
        let hashFile = crypto.createHash('sha1');
        hashFile.setEncoding('hex');
        fd.on('end', function() {
          hashFile.end();

          let hashFileAt = hashFile.read();
          console.log("HASH FILE: ",hashFileAt);
          // the desired sha1sum
          let company = req.body;


            dao.findByName(company.name)
              .then((cmp) => {
                console.log("CMP j: ",cmp);
                //let msg = { 'message': `Company updated: ${cmp.name}` };
                if(cmp.competitorTx!=null){
                  dao.update(company.name, {"winnerHash2": hashFileAt})
                    .then((cmp) => {
                      console.log("CMP: ",cmp);
                      //let msg = { 'message': `Company updated: ${cmp.name}` };

                    } )
                    .catch((err) => {
                      if(err){
                        if(err.code === 11000){
                          //let msg = `Company name is duplicated: ${company.name}`;
                          console.log("err: ",err.code);


                        }
                        next(err);
                      }
                    });
                    if(cmp.winnerHash1!=null){
                      let dataCertificate = cmp.name+','+cmp.attorney+','+cmp.email+','+cmp.nationality+','+cmp.hyperlink+','+cmp.winnerHash1+cmp.winnerHash2+cmp.winnerHash3;
                      competitor.setWinnerCertificate(company.name, dataCertificate);

                    }
                }



              } )
              .catch((err) => {
                if(err){
                  if(err.code === 11000){
                    //let msg = `Company name is duplicated: ${company.name}`;
                    console.log("err: ",err.code);


                  }
                  next(err);
                }
              });
        });
        // read all file and pipe it (write it) to the hash object
        fd.pipe(hashFile);




        return res.json({ success: true, filename: f.filename/*, filepath: path*/});

			} catch (err) {
				console.error("Try-catch Error %s", err);
				console.trace(err);
				return res.json({ success: false,  message : err.message });
			}
		})
    .post('/winner3',upload.single('image'),	(req, res, next) => {
			try {
				//let imgMatrix;
        //console.log("app: ",router);
				console.log(JSON.stringify(req.headers));
        //console.log("filex: ",req);
				const f = req.file;
        console.log("F: ",f);
				if(!f){
					return res.json({ success: false, message: "File not received" });
				}
				//const filepath = f.destination + '/' + f.filename;
				//console.log("Temp file: %s %s", filepath, f.mimetype);
				/*if (router.images.mimetypes.indexOf(f.mimetype) < 0) {
					throw new Error( 'Invalid file - please upload an image (.jpg, .png, .gif).');
				}*/
        let path = f.destination+'/'+f.filename;
				console.log("Reading");
        //let certificateStatus;
        let fd = fs.createReadStream(path);
        let hashFile = crypto.createHash('sha1');
        hashFile.setEncoding('hex');
        fd.on('end', function() {
          hashFile.end();

          let hashFileAt = hashFile.read();
          console.log("HASH FILE: ",hashFileAt);
          // the desired sha1sum
          let company = req.body;


            dao.findByName(company.name)
              .then((cmp) => {
                console.log("CMP j: ",cmp);
                //let msg = { 'message': `Company updated: ${cmp.name}` };
                if(cmp.winnerTx==null&&cmp.competitorTx!=null){
                  dao.update(company.name, {"winnerHash3": hashFileAt})
                    .then((cmp) => {
                      console.log("CMP: ",cmp);
                      //let msg = { 'message': `Company updated: ${cmp.name}` };

                    } )
                    .catch((err) => {
                      if(err){
                        if(err.code === 11000){
                          //let msg = `Company name is duplicated: ${company.name}`;
                          console.log("err: ",err.code);


                        }
                        next(err);
                      }
                    });
                    if(cmp.winnerHash1!=null&&cmp.winnerHash2!=null){
                      let dataCertificate = cmp.name+','+cmp.attorney+','+cmp.email+','+cmp.nationality+','+cmp.hyperlink+','+cmp.winnerHash1+cmp.winnerHash2+cmp.winnerHash3;
                      competitor.setWinnerCertificate(company.name, dataCertificate);

                    }
                }



              } )
              .catch((err) => {
                if(err){
                  if(err.code === 11000){
                    //let msg = `Company name is duplicated: ${company.name}`;
                    console.log("err: ",err.code);


                  }
                  next(err);
                }
              });
        });
        // read all file and pipe it (write it) to the hash object
        fd.pipe(hashFile);




        return res.json({ success: true, filename: f.filename/*, filepath: path*/});

			} catch (err) {
				console.error("Try-catch Error %s", err);
				console.trace(err);
				return res.json({ success: false,  message : err.message });
			}
		});
	});


  /*router.get('/', (req, res) => {
    res.send('hello world');
  });*/

  return router;
}
