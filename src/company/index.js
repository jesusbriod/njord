import express from 'express';
import * as dao from './company.dao';

const router = express.Router();

export default function(){
  router.use((req, res, next) => {
    res.append('Last-Modified', Date.now());
    next();
  })
  .post('/', (req, res, next) => {
    try {
      let company = req.body;
      console.log("company: ",company);
      dao.createCompany(company)
        .then((cmp) => {
          let msg = { 'message': `Company created: ${cmp.name}` };
          res.status(201).send(msg);
        })
        .catch((err) => {
          if(err){
            if(err.code === 11000){
              let msg = { 'message': `Company is duplicated: ${company.name}` };
              res.status(400).send(msg);
            }
            next(err);
          }
        });
    } catch(err) {
      next(err);
    }
  })
  .patch('/:id', (req, res, next) => {
    try{
      let company = req.body;
      dao.updateCompany(req.params.id, company)
        .then((cmp) => {
          let msg = { 'message': `Company updated: ${cmp.name}` };
          res.status(200).send(msg);
        } )
        .catch((err) => {
          if(err){
            if(err.code === 11000){
              let msg = `Company name is duplicated: ${company.name}`;
              res.status(400).send({ 'message': msg });
            }
            next(err);
          }
        });
    } catch(err) {
      next(err);
    }
  }).delete('/:id', (req, res, next) => {
    try{
      dao.deleteCompany(req.params.id)
        .then(() => {
          let msg = { 'message': `Company deleted` };
          res.status(200).send(msg);
        } )
        .catch(next);
    } catch(err) {
      next(err);
    }
  })
  .get('/:id?', (req, res, next) => {
    try{
      dao.findCompany(req.params.id)
        .then((rst) => {
          res.status(200).json(rst);
        })
        .catch(next);
    } catch(err) {
      next(err);
    }
  });

  return router;
}
