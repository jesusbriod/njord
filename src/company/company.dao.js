import mongoose from 'mongoose';

const CompanySchema = mongoose.Schema({
  name: { type: String, unique: true },
  attorney: String,
  email: String,
  founders: [String],
  nationality: String,
  hyperlink: String,
  status: { type: String, enum: ['registered', 'promoted'], default: 'registered' },
  updated: { type: Date, default: Date.now },
  competitorHash: {type: String, default: null },
  winnerHash1: {type: String, default: null},
  winnerHash2: {type: String, default: null},
  winnerHash3: {type: String, default: null},
  competitorTx: {type: String, default: null },
  winnerTx: {type: String, default: null },
  competitorStatus: {type: String, default: null},
  winnerStatus: {type: String, default: null}
});

const CompanyModel = mongoose.model('Company', CompanySchema);

export function createCompany(company){
  return (new CompanyModel(company)).save();
}

export function findCompany(id){
  return ((id)? CompanyModel.findById(id) : CompanyModel.find() ).select('-__v');
}

export function updateCompany(id, company){
  return CompanyModel.findByIdAndUpdate(id, company, {upsert: true});
}

export function deleteCompany(id){
  return CompanyModel.findByIdAndRemove(id);
}

export function findByName(name){
  return (CompanyModel.findOne({"name":name}));
}

export function update(name, company){
  return CompanyModel.update({"name":name}, company);
}

export function findAllOneColumn(){
  return CompanyModel.find({}, {name:1, competitorTx:1, winnerTx:1});
}
